module SplitSpec where

import Test.Hspec
import Test.QuickCheck

import Split

splitSpec0 = do
  describe "split" $ do 
    it "splits a string wrt. a given character into a list of words" $
      (split '/' "aa/bb/ccc/dd d") `shouldBe` ["aa", "bb", "ccc", "dd d"]
      -- split va séparer la chaîne de caractères en fonction du caractère '/' et renvoyer une liste de mots

splitSpec1 = do
  describe "split" $ do
    it "can be undone with unsplit (v1)" $ property $
      \c xs -> collect (length xs) $ -- on affiche les tailles des différentes listes arbiitraire
        prop_split_unsplit c xs -- on vérifie la propriété pour des caractères et des listes d'entiers arbitraires 


splitSpec2 = do
  describe "split" $ do
    it "can be undone with unsplit (v2)" $ property $
    -- elements :: [a] -> Gen a
    -- Generates one of the given values. The input list must be non-empty.
    -- forAll :: (Show a, Testable prop) => Gen a -> (a -> prop) -> Property
    --Explicit universal quantification: uses an explicitly given test case generator.
      \xs -> forAll (elements xs) $  -- xs est une chaine de caractères générarée aléatoirement
      -- le forall prend un générateur de caractères aléatoires et vérifie la propriété pour ce caractère
        \c -> collect (length xs) $ prop_split_unsplit c xs 


-- Remarque : on utilise comme caractère de split les éléments des listes `xs` en entrée,
--            cf. la doc QuickCheck sur `forAll`, `elements`, etc.


splitSpec3 = do
  describe "split" $ do
    it "can be undone with unsplit (v3)" $ property $
    -- oneof va donc choisir un générateur parmi une liste de générateurs
    -- et le passer à forAll pour générer une chaîne de caractères aléatoire      
      forAll (oneof [return "bla bla bli" 
                    , return "toto"
                    , return ""
                    , return "un    deux trois   quatre"]) $
      \xs -> prop_split_unsplit ' ' xs

