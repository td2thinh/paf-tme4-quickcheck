module Bridge where


-- IslandBridge est un data type d'un pont reliant un continent à une île
-- Il est soit ouvert, soit fermé
-- Dans le cas où il est ouvert, on a le nombre de voitures vers l'île, 
-- le nombre de voitures sur l'île et le nombre de voitures vers le continent
-- Dans le cas où il est fermé, on a le nombre de voitures vers l'île et le nombre de voitures vers le continent
-- A un moment donné, on ne peut pas avoir plus de voitures que la limite sur le pont
data IslandBridge =
  BridgeOpened Int Int Int Int 
  | BridgeClosed Int Int Int
  deriving Show

-- smart constructor
mkBridge :: Int -> Int -> Int -> Int -> IslandBridge
mkBridge lim nbTo nbI nbFrom
  | nbTo + nbI + nbFrom < lim = BridgeOpened lim nbTo nbI nbFrom
  | nbTo + nbI + nbFrom == lim = BridgeClosed lim nbTo nbFrom
  | otherwise = error "Wrong bridge (limit exceeded)"

-- accessors
nbCarsOnIsland :: IslandBridge -> Int
nbCarsOnIsland (BridgeOpened _ _ nbI _) = nbI
nbCarsOnIsland (BridgeClosed lim nbTo nbFrom) = lim - (nbFrom + nbTo)

nbCarsToIsland :: IslandBridge -> Int
nbCarsToIsland (BridgeOpened _ nbTo _ _) = nbTo
nbCarsToIsland (BridgeClosed _ nbTo _) = nbTo

nbCarsFromIsland :: IslandBridge -> Int
nbCarsFromIsland (BridgeOpened _ _ _ nbFrom) = nbFrom
nbCarsFromIsland (BridgeClosed _ _ nbFrom) = nbFrom

nbCars :: IslandBridge -> Int
nbCars (BridgeOpened _ nbTo nbI nbFrom) = nbTo + nbI + nbFrom
nbCars (BridgeClosed lim _ _) = lim

bridgeLimit :: IslandBridge -> Int
bridgeLimit (BridgeOpened lim _ _ _) = lim
bridgeLimit (BridgeClosed lim _ _) = lim

-- invariant
prop_inv_IslandBridge :: IslandBridge -> Bool
prop_inv_IslandBridge b@(BridgeOpened lim _ _ _) =
  0 <= nbCars b && nbCars b <= lim
prop_inv_IslandBridge b@(BridgeClosed lim _ _) =
  nbCars b == lim

-- initialisation
initBridge :: Int -> IslandBridge
initBridge lim = mkBridge lim 0 0 0

prop_pre_initBridge :: Int -> Bool
prop_pre_initBridge lim = lim > 0

-- Une fois le pont est fermé, on ne peut pas entrer sur le pont dans les deux sens

-- opération : entrée du continent vers l'île
enterToIsland :: IslandBridge -> IslandBridge
enterToIsland (BridgeOpened lim nbTo nbI nbFrom) = mkBridge lim (nbTo+1) nbI nbFrom
enterToIsland BridgeClosed{} = error "Cannot enter bridge to island"
prop_pre_enterToIsland :: IslandBridge -> Bool
prop_pre_enterToIsland b@(BridgeOpened lim _ _ _) = nbCars b < lim
prop_pre_enterToIsland BridgeClosed{} = False

-- opération : permet à un véhicule de quitter le pont vers l'île
leaveToIsland :: IslandBridge -> IslandBridge
leaveToIsland (BridgeOpened lim nbTo nbI nbFrom) = mkBridge lim (nbTo-1) (nbI+1) nbFrom
leaveToIsland (BridgeClosed lim nbTo nbFrom) = mkBridge lim (nbTo-1) (lim - nbTo - nbFrom + 1) nbFrom

prop_pre_leaveToIsland :: IslandBridge -> Bool
prop_pre_leaveToIsland b@(BridgeOpened lim nbTo _ _) = nbTo > 0 && nbCars b < lim
prop_pre_leaveToIsland (BridgeClosed lim nbTo nbFrom) = nbTo + nbFrom == lim

-- opération : permet à un véhicule d'entrer sur le pont depuis l'île
enterFromIsland :: IslandBridge -> IslandBridge
enterFromIsland (BridgeOpened lim nbTo nbI nbFrom) = mkBridge lim nbTo (nbI-1) (nbFrom+1)
enterFromIsland (BridgeClosed lim nbTo nbFrom) = mkBridge lim nbTo (lim - nbTo - nbFrom - 1) (nbFrom+1)

prop_pre_enterFromIsland :: IslandBridge -> Bool
prop_pre_enterFromIsland b@(BridgeOpened lim _ nbI _) = nbI > 0 && nbCars b < lim
prop_pre_enterFromIsland (BridgeClosed lim nbTo nbFrom) = nbTo + nbFrom < lim

-- opération : permet à un véhicule de quitter le pont vers le continent
leaveFromIsland :: IslandBridge -> IslandBridge
leaveFromIsland b@(BridgeOpened lim nbTo nbI nbFrom) = mkBridge lim nbTo nbI (nbFrom-1)
leaveFromIsland BridgeClosed{} = error "Cannot leave bridge from island"

prop_pre_leaveFromIsland :: IslandBridge -> Bool
prop_pre_leaveFromIsland b@(BridgeOpened _ _ _ nbFrom) = nbFrom > 0
prop_pre_leaveFromIsland BridgeClosed{} = False
